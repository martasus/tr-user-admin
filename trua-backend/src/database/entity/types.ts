enum Privileges {
    ADMIN = "admin",
    COMPANY = "company",
    REGULAR = "regular"
}

export {
    Privileges
}